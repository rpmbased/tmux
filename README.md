# tmux

RPMs for terminal multiplexer

## homep

https://github.com/tmux/tmux

## centos6

- Download tmux rpm file for **centos6** from here:
    - [tmux - centos6](https://gitlab.com/rpmbased/tmux/-/jobs/artifacts/master/browse?job=build-centos-6)

## centos7

- Download tmux rpm file for **centos7** from here:
    - [tmux - centos7](https://gitlab.com/rpmbased/tmux/-/jobs/artifacts/master/browse?job=build-centos-7)
